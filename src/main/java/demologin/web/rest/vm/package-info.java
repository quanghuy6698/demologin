/**
 * View Models used by Spring MVC REST controllers.
 */
package demologin.web.rest.vm;
